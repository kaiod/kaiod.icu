---
layout: page
---

# Políticas

> As políticas abaixo são referentes ao site e seu conteúdo. Portanto, não a considere fora deste domínio!

## Política de privacidade
Este site não coleta nenhuma informação do visitante. Nos dedicamos a eliminar **qualquer informação** para resguardar o seu direito a privacidade e anonimidade, portanto, utilizamos somente provedores (tanto de hospedagem, quanto demais serviços) que coletem somente as **informações necessárias**, sendo excluídas após determinado prazo. 

Para mais informações, veja a política da [Framagit](https://framagit.org/-/users/terms) e [Cloudflare](https://www.cloudflare.com/pt-br/privacypolicy/).

> Nota: Em breve, os serviços da Cloudflare não serão mais utilizados.

## Política de direitos autorais
- **Artigos, notas e outros:** Todos os direitos reservados. Este conteúdo ou qualquer parte dele não pode ser reproduzido ou usado de forma alguma sem autorização expressa, por escrito, do autor ou editor, exceto pelo uso de citações breves.

- **Mídias:** Somente quando específicado, todas as imagens, vídeos ou demais conteúdos relacionados, devem seguir uma política de [licença livre](https://creativecommons.org/share-your-work/public-domain/freeworks/). Com exceção de materiais que sirvam de ilustação e fácil compreensão de artigos ou notas.

- **Favicon**: O favicon deste site está licenciado sob [domínio público](https://creativecommons.org/publicdomain/mark/1.0/deed.pt_BR). Você pode encontrar-lo no site [Open Clipart](https://openclipart.org/detail/330647/confused-boy).

- **Licença de software:** O código-fonte deste site está disponível sob [licença MIT](https://framagit.org/kaiod/kaiod.icu/-/blob/master/LICENSE.md).  

## Política de padronização
As padronizações abertas colaboram nas comunicações e no acesso a informação. Portanto, implementamos e seguimos alguns protocolos que podem ser úteis para robôs e usuários:

* [Web Robots (robots.txt)](https://www.robotstxt.org/orig.html)
* [Security.txt - RFC 9116](https://securitytxt.org/)
* [Schema.org](https://schema.org/docs/feeds.html)
* [Well Known - RFC 8615](https://datatracker.ietf.org/doc/html/rfc8615)
* [RSS - Atom Syndication Format](https://www.ietf.org/rfc/rfc4287.txt)
