---
layout: page
---

# Chave PGP

> Você pode saber mais sobre as **chaves PGP** no [Guia de Autodefesa contra Vigilância da EFF](https://ssd.eff.org/pt-br/module/uma-introdu%C3%A7%C3%A3o-%C3%A0-criptografia-de-chave-p%C3%BAblica-e-pgp) ou no site oficial do [GnuPG](https://gnupg.org/).

## Minha atual chave PGP:

* **ID da chave:** 475D7F7857DDE1A0
* **Criada em:** 24/05/2022
* **Impressão digital da chave:** 1C E1 C3 19 71 25 B8 B9 55 5E 6A FB 47 5D 7F 78 57 DD E1 A0
* **UID principal do usuário:** kaiod@riseup.net
* **ID da sub-chave pública:** 925316129F186D25

[> Baixar chave](https://kaiod.icu/kaiod-pubkey.txt)
